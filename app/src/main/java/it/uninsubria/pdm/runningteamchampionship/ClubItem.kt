package it.uninsubria.pdm.runningteamchampionship

class ClubItem {
    var clubName: String? = ""
    var fidalCode: String? = ""
    var clubMail: String? = ""
    var clubWebsite: String? = ""

    constructor(club: String?,fidal: String?,mail: String?,www: String?){
        clubName = club
        fidalCode = fidal
        clubMail = mail
        clubWebsite= www
    }

    override fun toString(): String {
        return clubName + " - "+ fidalCode + " - " + clubMail + " - " + clubWebsite
    }
}