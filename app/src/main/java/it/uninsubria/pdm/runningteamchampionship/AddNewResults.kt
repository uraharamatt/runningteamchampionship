package it.uninsubria.pdm.runningteamchampionship

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_add_new_results.*
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.tool_bar.*

class AddNewResults : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_new_results)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.title = "Nuovo Risultato"
    }
    override fun onSupportNavigateUp(): Boolean {
        val resultIntent = Intent()
        val hourDefault = "00"
        val athleteName = editName.text.toString() + " " + editSurname.text.toString()
        //data check
        if (athleteName.isEmpty()) {
            editName.error = "Inserisci il Nome e il Cognome"
            return false
        }
        if (editHour.text.toString() == "") {
            editHour.setText(hourDefault)
        }
        if (editHour.text.toString() == "" || editMinute.text.toString() == "" || editSecond.text.toString() == ""){
            editMinute.error = "Inserisci il tempo correttamente, 00 se non presenti le ore"
            return false
        }
        val raceTime = editHour.text.toString() + ":" + editMinute.text.toString() + "." + editSecond.text.toString()
        if (editDistance.text.isEmpty()) {
            editDistance.error = "Inserisci la distanza Gara"
            return false
        }
        if (editPlace.text.isEmpty()) {
            editDistance.error = "Inserisci il nome della gara"
            return false
        }
        //return data to backintent
        resultIntent.putExtra("NOME_COGNOME", athleteName)
        resultIntent.putExtra("PLACE_of_RACE", editPlace.text.toString())
        resultIntent.putExtra("RACE_DISTANCE", editDistance.text.toString())
        resultIntent.putExtra("RACE_TIME", raceTime)
        setResult(Activity.RESULT_OK, resultIntent)
        finish()
        return true
    }
}