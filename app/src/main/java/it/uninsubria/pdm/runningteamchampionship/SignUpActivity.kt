package it.uninsubria.pdm.runningteamchampionship

import android.app.DatePickerDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Switch
import android.widget.Toast
import android.widget.ToggleButton
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_register.*
import java.util.*
import kotlin.collections.HashMap

class SignUpActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private var TAG = "SignUpActivity"
    val CLUB_REQUEST = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        auth = Firebase.auth
        //date Picking
        editTextDate.setOnClickListener {
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)
            val dpd = DatePickerDialog(this, DatePickerDialog
                .OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    // Display Selected date in textbox
                    editTextDate.text = ("" + dayOfMonth + "/" + (monthOfYear + 1) + "/" + year)
                }, year, month, day
            )
            dpd.show()
        }
        registerButton.setOnClickListener{
            onSignUpClick()
        }
    }

    private fun onSignUpClick() {
        val email = emailEditText.text.toString().trim()
        val password = passwordEditText.text.toString().trim()
        val userName = nameEditText.text.toString().trim()
        val userSurname = surNameEditText.text.toString().trim()
        val dateBorn = editTextDate.text.toString().trim()
        if (userName.isEmpty()) {
            nameEditText.error = "Inserisci il Nome"
            return
        }
        if (userSurname.isEmpty()) {
            surNameEditText.error = "Inserisci il Cognome"
            return
        }
        if (email.isEmpty()) {
            emailEditText.error = "Inserisci l email"
            return
        }
        if (password.isEmpty()) {
            passwordEditText.error = "Inserisci la password"
            return
        }
        if (password.length < 6){
            passwordEditText.error = "la password deve essere maggiore di 6 caratteri"
            return
        }
        var presidente = false
        val switch: ToggleButton = findViewById(R.id.switchPresidente)
        if (switch.isChecked)
            presidente = true
        createUser(userName, userSurname,dateBorn,email,password,presidente)
    }

    private fun createUser(userName: String, userSurname: String, dateBorn: String, email: String, password: String ,presidente: Boolean) {
        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "createUserWithEmail:success")
                    val currentlyUser = auth.currentUser
                    val uid = currentlyUser!!.uid
                    val newUser = User(userName,userSurname,email,dateBorn,presidente)
                    val database = Firebase.database.reference
                    database.child("users").child(uid).setValue(newUser)
                        .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            //if user is a President start the activity of creation of a new Club
                            if(presidente){
                                val intent = Intent(applicationContext, AddNewClub::class.java)
                                startActivity(intent)
                                finish()
                            }else{
                                val intent = Intent(applicationContext, ChooseClub::class.java)
                                startActivity(intent)
                                finish()
                            }
                        }
                    }
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(TAG, "createUserWithEmail:failure", task.exception)
                    Toast.makeText(baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT).show()
                }
            }
    }
    fun loginRedirect(view: View) {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }
}
