package it.uninsubria.pdm.runningteamchampionship

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_add_new_club.*
import kotlinx.android.synthetic.main.activity_register.*

class AddNewClub : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private var TAG = "AddNewClubActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_new_club)
        auth = Firebase.auth
    }
    override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null)
        val currentUser = auth.currentUser
        if (currentUser == null) {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        } else {
            createClubButton.setOnClickListener{
                onCreateCLub()
            }
        }
    }

    private fun onCreateCLub() {
        val clubName = editClubName.text.toString().trim()
        val fidalCode = editClubFIDALCode.text.toString().trim()
        val clubMail = editMailClub.text.toString().trim()
        val clubWebsite = editSiteClub.text.toString().trim()
        if (clubName.isEmpty()) {
            nameEditText.error = "Inserisci il Nome"
            return
        }
        if (fidalCode.isEmpty()) {
            surNameEditText.error = "Inserisci il Cognome"
            return
        }
        if (clubMail.isEmpty()) {
            emailEditText.error = "Inserisci l email"
            return
        }
        createClub(clubName, fidalCode,clubMail,clubWebsite)
    }
    private fun createClub(club: String?,fidal: String?,mail: String?,www: String?) {
        val clubNew = ClubItem(club, fidal, mail, www)
        val database = Firebase.database.reference
        val currentUser = auth.currentUser?.uid.toString()

        database.child("clubs").push().setValue(clubNew)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    database.child("users").child(currentUser).child("club").push().setValue(club.toString())
                        .addOnCompleteListener { task ->
                            if (task.isSuccessful) {
                                Toast.makeText(
                                    baseContext, "Club Assegnato.",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                    Toast.makeText(
                        baseContext, "Adding Club Complete.",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        Toast.makeText(this, clubNew.toString() + "Added to firebaseDB", Toast.LENGTH_LONG).show()
        val intent = Intent(applicationContext, MainActivity::class.java)
        startActivity(intent)
        finish()
    }
}