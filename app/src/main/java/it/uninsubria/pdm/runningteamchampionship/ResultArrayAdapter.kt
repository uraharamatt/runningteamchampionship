package it.uninsubria.pdm.runningteamchampionship

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import java.util.ArrayList

class ResultArrayAdapter(private var activity: Activity, private var items: ArrayList<ResultItem>) :  BaseAdapter(){

    private class ViewHolder(row: View?) {
        var athlete: TextView? = null
        var place: TextView? = null
        var distance: TextView? = null
        var time: TextView? = null
        init {
            this.athlete = row?.findViewById(R.id.athleteNameView)
            this.place = row?.findViewById(R.id.placeNameView)
            this.distance = row?.findViewById(R.id.distanceView)
            this.time = row?.findViewById(R.id.timeShow)
        }
    }
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View
        val viewHolder: ViewHolder
        if (convertView == null) {
            val inflater = activity?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = inflater.inflate(R.layout.list_item, null)
            viewHolder = ViewHolder(view)
            view.tag = viewHolder
        } else {
            view = convertView
            viewHolder = view.tag as ViewHolder
        }
        var res = items[position]
        viewHolder.athlete?.text = res.athlete
        viewHolder.place?.text = res.racename
        viewHolder.distance?.text = res.distance
        viewHolder.time?.text = res.racetime
        return view
    }
    override fun getItem(i: Int): ResultItem {
        return items[i]
    }
    override fun getItemId(i: Int): Long {
        return i.toLong()
    }
    override fun getCount(): Int {
        return items.size
    }
}