package it.uninsubria.pdm.runningteamchampionship

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_choose_club.*
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_login.club_spinner


class LoginActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private var TAG = "LoginActivity"
    private lateinit var mFirebaseUser: FirebaseUser
    private lateinit var mDatabase: DatabaseReference
    private lateinit var mUserId: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        auth = Firebase.auth
        spinnerCreation()
        loginButton.setOnClickListener{
            onLoginClick()
        }
        signUpTextView.setOnClickListener {
            onSignUpClick()
        }
        forgottenPasswordTextView.setOnClickListener{
            onRecoveryPasswordClick()
        }
    }
    private fun onSignUpClick() {
        val intent = Intent(this, SignUpActivity::class.java)
        startActivity(intent)
        finish()
    }
    private fun onRecoveryPasswordClick() {
        /*val intent = Intent(this, RecoveryPassword::class.java)
        startActivity(intent)
        finish()*/
    }
    private fun onLoginClick() {
        val email = emailEditText.text.toString().trim()
        val password = passwordEditText.text.toString().trim()
        if (email.isEmpty()) {
            emailEditText.error = "Enter email"
            return
        }
        if (password.isEmpty()) {
            passwordEditText.error = "Enter password"
            return
        }
        loginUser(email, password)
    }

    private fun loginUser(email: String, password: String) {

        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    val currentlyUser = auth.currentUser
                    val uid = currentlyUser!!.uid
                    val database = Firebase.database.reference
                    val ref = database.child("users").child(uid).child("club")
                    ref.addValueEventListener(object : ValueEventListener {
                        override fun onCancelled(p0: DatabaseError) {
                            TODO("Not yet implemented")
                        }
                        override fun onDataChange(dataSnapshot: DataSnapshot) {
                            // Get club value and control the spinner
                            val club = dataSnapshot.value
                            if(club == club_spinner.toString()){
                                Log.d(TAG, "signInWithEmail:success")
                                val intent = Intent(applicationContext, MainActivity::class.java)
                                startActivity(intent)
                                finish()
                            }else{
                                Log.d(TAG, "Club Errato, Se sei Presidente registrane uno nuovo")
                                val intent = Intent(applicationContext, SignUpActivity::class.java)
                                startActivity(intent)
                                finish()
                            }

                        }
                    })
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(TAG, "signInWithEmail:failure", task.exception)
                    val builder = AlertDialog.Builder(this)
                    with(builder)
                    {
                        setTitle("Authentication failed")
                        setMessage(task.exception?.message)
                        setPositiveButton("OK", null)
                        show()
                    }
                }
            }
    }
    fun logOut(view: View) {
        FirebaseAuth.getInstance().signOut()
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }
    fun editRankings(view: View) {
        val intent = Intent(this, LeaderboardActivity::class.java)
        startActivity(intent)
        finish()
    }
    fun loginRedirect(view: View) {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }
    private fun spinnerCreation(){
        val clubSpinner = club_spinner
        val database = Firebase.database.reference
        database.child("clubs").addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val clubs: ArrayList<String> = ArrayList()
                for (dataSnapshot in dataSnapshot.children) {
                    val clubName = dataSnapshot.child("clubName").getValue(String::class.java)!!
                    clubs.add(clubName)
                }
                val clubsAdapter = ArrayAdapter(
                    this@LoginActivity,
                    android.R.layout.simple_spinner_item,
                    clubs
                )
                clubsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                clubSpinner.adapter = clubsAdapter
            }
            override fun onCancelled(databaseError: DatabaseError) {}
        })
    }
}
