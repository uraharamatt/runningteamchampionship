package it.uninsubria.pdm.runningteamchampionship

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_choose_club.*


class ChooseClub : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    val TAG = "Selezione Club"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_new_club)
        // Initialize Firebase Auth
        auth = Firebase.auth
    }
    override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null)
        val currentUser = auth.currentUser
        if (currentUser == null) {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        } else {
            //adding club list to spinner
            spinnerCreation()
            club_select_button.setOnClickListener{
                onClubSelectClick()
            }
        }
    }
    private fun spinnerCreation(){
        val clubSpinner = club_spinner
        val database = Firebase.database.reference
        database.child("clubs").addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val clubs: ArrayList<String> = ArrayList()
                for (dataSnapshot in dataSnapshot.children) {
                    val clubName = dataSnapshot.child("clubName").getValue(String::class.java)!!
                    clubs.add(clubName)
                }
                val clubsAdapter = ArrayAdapter(
                    this@ChooseClub,
                    android.R.layout.simple_spinner_item,
                    clubs
                )
                clubsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                clubSpinner.adapter = clubsAdapter
            }
            override fun onCancelled(databaseError: DatabaseError) {}
        })
    }

    private fun onClubSelectClick() {
        //selezione dallo spinner
        val club = club_spinner.toString().trim()
        val currentlyUser = auth.currentUser
        val uid = currentlyUser!!.uid
        val database = Firebase.database.reference
        database.child("users").child(uid).child("club").push().setValue(club)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Toast.makeText(baseContext, "Adding club Complete.",
                        Toast.LENGTH_SHORT).show()
                }
                else {
                    // If club choose fails, display a message to the user.
                    Log.w(TAG, "Fail Selection of Club", task.exception)
                    Toast.makeText(baseContext, "Selection failed.",
                        Toast.LENGTH_SHORT).show()
                }
            }
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }
}