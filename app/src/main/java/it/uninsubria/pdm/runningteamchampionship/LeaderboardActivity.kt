package it.uninsubria.pdm.runningteamchampionship

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.tool_bar.*
import java.util.*
import kotlin.collections.ArrayList
import kotlin.properties.Delegates


class LeaderboardActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    var scoreItems: ArrayList<ResultItem> = ArrayList()
    var adapter : ResultArrayAdapter by Delegates.notNull()
    val NEW_ITEM_REQUEST = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_leaderboard)
        // Initialize Firebase Auth
        auth = Firebase.auth
        // Sets the Toolbar to act as the ActionBar
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        // Display icon in the toolbar, next->implements logo of club
        supportActionBar?.setLogo(R.mipmap.ic_launcher)
        supportActionBar?.setDisplayUseLogoEnabled(true)
        // get the references to the views
        val listView: ListView = findViewById(R.id.list_view)
        //Get datasnapshot at "results" root node
        //next TODO -> add a recovery of results of right Club of inscription
        FirebaseDatabase.getInstance().reference.child("results").orderByChild("racetime")
            .addChildEventListener(object : ChildEventListener {
                override fun onChildAdded(dataSnapshot: DataSnapshot, s: String?) {
                    val result = ResultItem(dataSnapshot.child("athlete").value as String?,
                            dataSnapshot.child("racename").value as String?,
                            dataSnapshot.child("distance").value as String?,
                            dataSnapshot.child("racetime").value as String?)
                    scoreItems.add(0,result)
                    Collections.sort(scoreItems,
                        Comparator { rs1, rs2 -> rs1.racetime.toString().compareTo(rs2.racetime.toString()) })
                    adapter.notifyDataSetChanged()
                }
                override fun onChildChanged(dataSnapshot: DataSnapshot,s: String?) {}
                override fun onChildRemoved(dataSnapshot: DataSnapshot) {
                    val result = ResultItem(dataSnapshot.child("athlete").value as String?,
                        dataSnapshot.child("racename").value as String?,
                        dataSnapshot.child("distance").value as String?,
                        dataSnapshot.child("racetime").value as String?)
                    scoreItems.remove(result)
                    adapter.notifyDataSetChanged()
                }
                override fun onChildMoved(dataSnapshot: DataSnapshot, s: String?) {}
                override fun onCancelled(databaseError: DatabaseError) {}
            })
        //adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, scoreItems)
        adapter = ResultArrayAdapter(this,scoreItems)
        // View items are loaded into the ListView container.
        listView.adapter = adapter
    }
    override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null)
        val currentUser = auth.currentUser
        if (currentUser == null) {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // Check which request we're responding to
        if (requestCode == NEW_ITEM_REQUEST) {
            // Make sure the request was successful
            if (resultCode == Activity.RESULT_OK) {
                val athleteName = data?.getStringExtra("NOME_COGNOME")
                val placeofRace = data?.getStringExtra("PLACE_of_RACE")
                val distance = data?.getStringExtra("RACE_DISTANCE")
                val raceTime = data?.getStringExtra("RACE_TIME")
                Log.d(MainActivity::class.java.name, "onActivityResult() -> $athleteName")
                addNewResult(athleteName,placeofRace,distance,raceTime)
                return
            }
        }
    }

    fun addNewResult(athleteName: String?, placeofRace: String?, distance: String?,raceTime:String?){
        val newResult = ResultItem(athleteName,placeofRace,distance,raceTime)
        val database = Firebase.database.reference// FirebaseDatabase.getInstance().getReference("users").child(uid)
        database.child("results").push().setValue(newResult)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Toast.makeText(baseContext, "Adding Result Complete.",
                        Toast.LENGTH_SHORT).show()
                }
            }
        scoreItems.add(0,newResult)
        adapter.notifyDataSetChanged()
        Toast.makeText(this, newResult.toString() + "Added to firebaseDB", Toast.LENGTH_LONG).show()
    }
    override fun onSupportNavigateUp(): Boolean {
        val intent = Intent(applicationContext, MainActivity::class.java)
        startActivity(intent)
        finish()
        return true
    }

    //Menu Function
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
    // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_leaderboard_activity, menu)
        return true
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        /*val currentUser = auth.currentUser.uid.toString()
        val ref = FirebaseDatabase.getInstance().reference.child("users").child(currentUser).child("presidente")
        ref.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                TODO("Not yet implemented")
            }
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // Get presidente value
                val president = dataSnapshot.value
                if(president == true){
                    R.id.action_new_item.visibility= View.VISIBLE
                    Club.visibility= View.VISIBLE
                }
            }
        })*/

        when (item.itemId) {
            R.id.action_settings -> {
                Toast.makeText(
                    applicationContext,
                    "Not yet implemented!Next Step Data Modification",
                    Toast.LENGTH_SHORT
                ).show()
                return true
            }
            R.id.action_new_item -> {
                val i = Intent(applicationContext, AddNewResults::class.java)
                startActivityForResult(i, NEW_ITEM_REQUEST)
                return true
            }
            R.id.action_logout -> {
                auth.signOut()
                val intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
