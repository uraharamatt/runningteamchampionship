package it.uninsubria.pdm.runningteamchampionship

import android.os.Build
import androidx.annotation.RequiresApi
import java.sql.Date
import java.time.Year

class ChampionshipItem {

    var champName: String? = ""
    //@RequiresApi(Build.VERSION_CODES.O)
    var champYear:Int? = 2020 //Year? = Year.now()
    var distance: String? = ""
    var club: String? = ""

    constructor(campionato: String?,year: Int?,km: String?,clubName: String?){
        champName = campionato
        champYear = year
        distance = km
        club = clubName

    }

    override fun toString(): String {
        return champName + "(" + "Anno:"+ champYear+") : " + "Distanza:" + distance
    }

}