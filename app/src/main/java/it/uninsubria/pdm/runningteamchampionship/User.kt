package it.uninsubria.pdm.runningteamchampionship

import com.google.firebase.database.IgnoreExtraProperties
import java.util.*

@IgnoreExtraProperties
data class User(
    var username: String? = "",
    var usersurname: String? = "",
    var email: String? = "",
    var borndate: String? = "",
    var presidente: Boolean ? = false,
    var club: String? = ""


)