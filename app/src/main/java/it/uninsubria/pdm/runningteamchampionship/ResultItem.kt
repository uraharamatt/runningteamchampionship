package it.uninsubria.pdm.runningteamchampionship

class ResultItem {
    var athlete : String? = ""
    var racename: String? = ""
    var distance: String? = ""
    var racetime : String? = ""
    var championship: String? = ""
    var club: String? = ""

    constructor(completeName: String?,race: String?,km: String?,result: String?,nameChampionship: String?,clubname: String?){
        athlete = completeName
        racename = race
        distance = km
        racetime= result
        championship = nameChampionship
        club = clubname
    }

    override fun toString(): String {
        //test - toedit
        return athlete + " - "+ racename + " " + distance + ": " + racetime + " Campionato: "+championship
    }
}